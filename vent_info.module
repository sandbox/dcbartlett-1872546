<?php
/**
 * @file
 * A Simple Module to Scrape and retrieve Charater Info for Legion Members' Characters
 */

/**
 * Implements hook_help.
 *
 * Displays help and module information.
 *
 * @param path 
 *   Which path of the site we're using to display help
 * @param arg 
 *   Array that holds the current path as returned from arg() function
 */
function vent_info_help($path, $arg) {
  switch ($path) {
    case "admin/help#vent_info":
      return '<p>' . t("Module to connect to and download information from a Ventrilo server.") . '</p>';
      break;
  }
}

/**
 * Implements hook_block_info().
 */
function vent_info_block_info() {
  $blocks['vent_info'] = array(
    'info' => t('Ventrilo Info'), //The name that will appear in the block list.
    'cache' => DRUPAL_CACHE_PER_ROLE, //Default
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 * 
 * Prepares the contents of the block.
 */
function vent_info_block_view($delta = '') {
  switch($delta){
    case 'vent_info':
      $result = vent_info_contents('block');
      $block['subject'] = t('Ventrilo Info');
      if(user_access('access content')){
      // $block['content'] = $result;
      }
    return $block;
  } 
}

/**
 * Custom page callback function, declared in vent_info_menu().
 */
function _vent_info_page() {
  $result = vent_info_contents('page');

  if ( $result->Request() )
  {
    $page_array['vent_info_arguments'] = array(
      '#title' => t('Error in Ventilo Info'),
      '#markup' => "CVentriloStatus->Request() failed. <strong>$result->m_error</strong><br><br>",
    );
  } else {
    dprint_r($result->m_clientlist);
    // Lets take each channel and get some info from it.  While we're at it, lets save that info
    //  with the needed html to the same channel in the array for display purposes.
    $new_channel = array();
    $i = 0;
    foreach ( $result->m_channellist as &$channel ) {
      $new_channel[$i] = "<div class='vent_channel'>$channel->m_name";
      foreach ( $result->m_clientlist as $user ) {
        if ( $user->m_cid == $channel->m_cid ) {
          $new_channel[$i] .= "<div class='vent_user'>";
          if ( $user->m_admin == 1 ) {
            $new_channel[$i] .= "<span class='vent_admin'>(A) </span>";
          }
          $new_channel[$i] .= $user->m_name ." ";
          if ( $user->m_comm ) {
            $new_channel[$i] .= "<span class='vent_admin'>[".$user->m_comm."] </span></div>";
          }
        }
      }
      $new_channel[$i] .= "</div>";
      $i++;
    }
    $page_array['vent_info_arguments'] = array(
      '#title' => t('Ventrilo Info'),
      '#markup' => "<div class='vent_info_table'>$result->m_name",
    );
    foreach ( $new_channel as $channel ) {
      $page_array['vent_info_arguments']['#markup'] .= $channel;
    }
    $page_array['vent_info_arguments']['#markup'] .= "</div>";
  }
  return $page_array;  
}

/**
 * Implements hook_menu()
 *
 * Prepares menu to page relations.
 */
function vent_info_menu() {
  $items = array();

  $items['admin/config/content/vent_info'] = array(
    'title' => 'Ventrilo Info',
    'description' => 'Configuration for Ventrilo Info module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vent_info_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['vent_info'] = array(
    'title' => 'Ventrilo Info',
    'page callback' => '_vent_info_page',
    'access arguments' => array('access vent_info content'),
    'type' => MENU_NORMAL_ITEM, //Will appear in Navigation menu.
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function vent_info_permission() {
  return array(
    'access vent_info content' => array(
      'title' => t('Access content for the Ventrilo Info module'),
    )
  );
}

/**
 * Page callback: Current posts settings
 *
 * @see vent_info_menu()
 */
function vent_info_form($form, &$form_state) {
  $form['vent_info_cmd'] = array(
    '#type' => 'textfield',
    '#title' => t('Ventrilo Status Command Location'),
    '#default_value' => variable_get('vent_info_cmd', "/usr/bin/ventrilo_status"),
    '#size' => 20,
    '#maxlength' => 255,
    '#description' => t('The location of the ventrilo_status Command.'),
    '#required' => TRUE,
  );
  $form['vent_info_ip'] = array(
    '#type' => 'textfield',
    '#title' => t('Server IP Address'),
    '#default_value' => variable_get('vent_info_ip', "127.0.0.1"),
    '#size' => 20,
    '#maxlength' => 15,
    '#description' => t('The ip address of the Ventrilo Server.'),
    '#required' => TRUE,
  );
  $form['vent_info_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Server Port'),
    '#default_value' => variable_get('vent_info_port', "3784"),
    '#size' => 20,
    '#maxlength' => 15,
    '#description' => t('The port of the Ventrilo Server.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Custom content function. 
 * 
 * Set beginning and end dates, retrieve posts from database
 * saved in that time period.
 * 
 * @return 
 *   A result set of the targeted posts.
 */
function vent_info_contents($display) {
  include_once "ventrilostatus.php";

  $stat = new CVentriloStatus;
  $stat->m_cmdprog  = variable_get('vent_info_cmd'); // Adjust accordingly.
  $stat->m_cmdcode  = "2";          // Detail mode.
  $stat->m_cmdhost  = variable_get('vent_info_ip');      // Assume ventrilo server on same machine.
  $stat->m_cmdport  = variable_get('vent_info_port');       // Port to be statused.
  $stat->m_cmdpass  = "";         // Status password if necessary.

  return $stat;

}